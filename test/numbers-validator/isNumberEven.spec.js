import { expect } from 'chai'
import { describe, beforeEach, afterEach, it } from 'mocha'
import NumbersValidator from '../../app/numbers_validator.js'


describe('Numbers validator', function() {

describe('isNumberEven', function () {
  let validator
  beforeEach(function () {
    validator = new NumbersValidator()
  })

  afterEach(function () {
    validator = null
  })

  it('should return true if number is even', function () {
    expect(validator.isNumberEven(4)).to.be.equal(true)
  })

  it('should throw an error when provided a string', function () {
    expect(() => validator.isNumberEven('4')).to.throw('[4] is not of type "Number" it is of type "string"')
  })
})

describe('getEvenNumbersFromArray', function () {
  let validator;

  beforeEach(function () {
    validator = new NumbersValidator();
  });

  afterEach(function () {
    validator = null;
  });

  it('should return an array of even numbers', function () {
    const inputArray = [1, 2, 3, 4, 5, 6];
    const expectedResult = [2, 4, 6];
    expect(validator.getEvenNumbersFromArray(inputArray)).to.deep.equal(expectedResult);
  });

  it('should throw an error when provided with a non-array', function () {
    expect(() => validator.getEvenNumbersFromArray('123')).to.throw('[123] is not an array of "Numbers"');
  });

  it('should throw an error when provided with an array containing non-numbers', function () {
    const inputArray = [1, '2', 3];
    expect(() => validator.getEvenNumbersFromArray(inputArray)).to.throw(`[${inputArray}] is not an array of "Numbers"`);
  });
});

describe('isAllNumbers', function () {
  let validator;

  beforeEach(function () {
    validator = new NumbersValidator();
  });

  afterEach(function () {
    validator = null;
  });

  it('should return true if all elements in the array are numbers', function () {
    const inputArray = [1, 2, 3];
    expect(validator.isAllNumbers(inputArray)).to.be.true;
  });

  it('should return false if the array contains a non-number', function () {
    const inputArray = [1, '2', 3];
    expect(validator.isAllNumbers(inputArray)).to.be.false;
  });

  it('should throw an error when provided with a non-array', function () {
    expect(() => validator.isAllNumbers('123')).to.throw('[123] is not an array');
  });
});

describe ('isInteger', function () {
  let validator;

  beforeEach(function () {
    validator = new NumbersValidator();
  });

  afterEach(function () {
    validator = null;
  });

  it('should return true if the value is an integer', function () {
    expect (validator.isInteger(4)).to.be.true;
  });

  it('should return false if the value is not an integer', function () {
    expect (validator.isInteger(4.5)).to.be.false;
  })

  it('it should throw an error when provided with non-number', function () {
    expect(() => validator.isInteger('4')).to.throw('[4] is not a number');
  })
})
})
